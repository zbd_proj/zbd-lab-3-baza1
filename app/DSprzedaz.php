<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DSprzedaz extends Model
{
    public $timestamps = false;

    protected $table = 'd_sprzedaz';
}
