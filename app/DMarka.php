<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DMarka extends Model
{
    public $timestamps = false;
    
    protected $table = 'd_marka';
}
