<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DSilnik extends Model
{
    public $timestamps = false;

    protected $table = 'd_silnik';
}
