<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DKlient extends Model
{
    public $timestamps = false;

    protected $table = 'd_klient';
}
