<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateDB extends Command
{
    protected $signature = 'generate:db';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('start:' . date('Y-m-d H:i:s'));

        $this->call('db:seed', [
            '--class' => 'ModelTableSeeder',
        ]);

        $silnikCount = \App\DSilnik::count();

        if ($silnikCount < 5000) {
            $this->call('db:seed', [
                '--class' => 'SilnikTableSeeder',
            ]);
        }

        $produkcjaCount = \App\DProdukcja::count();

        if ($produkcjaCount < 5000) {
            $this->call('db:seed', [
                '--class' => 'ProdukcjaTableSeeder',
            ]);
        }

        $this->call('db:seed', [
            '--class' => 'DanielTablesSeeder',
        ]);

        $this->info('end:' . date('Y-m-d H:i:s'));
    }
}
