<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DSamochod extends Model
{
    public $timestamps = false;

    protected $table = 'd_samochod';
}
