<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DCzas extends Model
{
    public $timestamps = false;

    protected $table = 'd_czas';
}
