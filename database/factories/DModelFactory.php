<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DMarka;
use App\DModel;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(DModel::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Fakecar($faker));
    $v = $faker->vehicleArray();

    $marka = DMarka::where('nazwa', $v['brand'])->first();

    if (!$marka) {
        $marka = DMarka::create([
            'nazwa' => $v['brand'],
        ]);
    }

    return [
        'marka_id' => $marka->id,
        'nazwa' => $v['model'],
    ];
});
