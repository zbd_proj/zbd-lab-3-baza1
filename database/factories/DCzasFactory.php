<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DCzas;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(DCzas::class, function (Faker $faker) {
    $monthNumber = $faker->month;

    return [
        'miesiac' => $monthNumber,
        'kwartal' => ceil($monthNumber / 3),
        'rok' => $faker->year,
        'dzien' => $faker->dayOfMonth,
        'dzien_tygodnia' => $faker->dayOfWeek,
        'godzina' => $faker->time($format = 'H:i', $max = 'now'),
    ];
});
