<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DSilnik;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(DSilnik::class, function (Faker $faker) {
    return [
        'moc' => $faker->numberBetween($min = 20, $max = 5000),
        'pojemnosc' => $faker->numberBetween($min = 1000, $max = 5000),
    ];
});
