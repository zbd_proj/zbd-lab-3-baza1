<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DKlient;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(DKlient::class, function (Faker $faker) {
    return [
        'wiek' => $faker->numberBetween($min = 18, $max = 60),
        'plec' => $faker->randomElement($array = array('male', 'female', '')),
        'zarobki' => $faker->numberBetween($min = 0, $max = 100000),
        'kraj_pochodzenia' => $faker->country,
        'wyksztalcenie' => $faker->randomElement($array = array('podstawowe', 'srednie', 'zawodowe', 'wyzsze', '')),
    ];
});
