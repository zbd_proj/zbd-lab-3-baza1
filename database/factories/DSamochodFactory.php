<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DModel;
use App\DProdukcja;
use App\DSamochod;
use App\DSilnik;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(DSamochod::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Fakecar($faker));

    $silnikCount = DSilnik::count();
    $modelCount = DModel::count();
    $produkcjaCount = DProdukcja::count();

    return [
        'kolor' => $faker->colorName,
        'paliwo' => $faker->vehicleFuelType,
        'przebieg' => $faker->numberBetween($min = 1000, $max = 500000),
        'skrzynia_biegow' => $faker->vehicleGearBoxType,
        'ilosc_drzwi' => $faker->numberBetween($min = 2, $max = 6),
        'ilosc_miejsc' => $faker->numberBetween($min = 2, $max = 20),
        'czy_powypadkowy' => $faker->numberBetween($min = 0, $max = 1),
        'silnik_id' => $faker->numberBetween($min = 1, $max = $silnikCount),
        'model_id' => $faker->numberBetween($min = 1, $max = $modelCount),
        'produkcja_id' => $faker->numberBetween($min = 1, $max = $produkcjaCount),

    ];
});
