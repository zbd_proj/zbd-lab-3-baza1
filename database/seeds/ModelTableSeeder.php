<?php

use App\DModel;
use Illuminate\Database\Seeder;

class ModelTableSeeder extends Seeder
{
    public function run()
    {
        for ($i = 0; $i < 500; $i++) {
            $model = factory(DModel::class)->make();

            $isExist = DModel::where('nazwa', $model->nazwa)->first();

            if (!$isExist) {
                $model->save();
            }
        }
    }
}
