<?php

use App\DSilnik;
use Illuminate\Database\Seeder;

class SilnikTableSeeder extends Seeder
{
    public function run()
    {
        factory(DSilnik::class, 3000)->create();
    }
}
