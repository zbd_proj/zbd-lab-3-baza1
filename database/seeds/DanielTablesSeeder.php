<?php

use App\DCzas;
use App\DKlient;
use App\DSamochod;
use Illuminate\Database\Seeder;

class DanielTablesSeeder extends Seeder
{
    public function run()
    {
        $ilosc = 50;

        factory(DSamochod::class, $ilosc)->create();
        factory(DKlient::class, $ilosc)->create();
        factory(DCzas::class, $ilosc)->create();

        $sprzedazCount = \App\DSprzedaz::count();

        factory(\App\DSprzedaz::class, $ilosc)
            ->make()
            ->each(function ($s, $index) use ($sprzedazCount) {
                $s['id_klient'] = $sprzedazCount + $index + 1;
                $s['id_samochod'] = $sprzedazCount + $index + 1;
                $s['id_czas'] = $sprzedazCount + $index + 1;

                $s->save();
            });
    }
}
