<?php

use App\DProdukcja;
use Illuminate\Database\Seeder;

class ProdukcjaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DProdukcja::class, 3000)->create();
    }
}
