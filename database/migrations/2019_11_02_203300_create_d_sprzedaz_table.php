<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDSprzedazTable extends Migration
{
    public function up()
    {
        Schema::create('d_sprzedaz', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cena');
            $table->unsignedBigInteger('id_klient');
            $table->unsignedBigInteger('id_samochod');
            $table->unsignedBigInteger('id_czas');
        });
    }

    public function down()
    {
        Schema::dropIfExists('d_sprzedaz');
    }
}
