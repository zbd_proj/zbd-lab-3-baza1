<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDSamochodTable extends Migration
{
    public function up()
    {
        Schema::create('d_samochod', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kolor');
            $table->string('paliwo');
            $table->string('przebieg');
            $table->string('skrzynia_biegow');
            $table->integer('ilosc_drzwi');
            $table->integer('ilosc_miejsc');
            $table->boolean('czy_powypadkowy');
            $table->bigInteger('silnik_id')->unsigned();
            $table->bigInteger('model_id')->unsigned();
            $table->bigInteger('produkcja_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('d_samochod');
    }
}
