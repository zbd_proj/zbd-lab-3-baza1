<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDCzasTable extends Migration
{
    public function up()
    {
        Schema::create('d_czas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('miesiac');
            $table->integer('kwartal');
            $table->year('rok');
            $table->integer('dzien');
            $table->string('dzien_tygodnia');
            $table->string('godzina');
        });
    }

    public function down()
    {
        Schema::dropIfExists('d_czas');
    }
}
