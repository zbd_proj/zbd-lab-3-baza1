<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDProdukcjaTable extends Migration
{
    public function up()
    {
        Schema::create('d_produkcja', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->year('rok');
            $table->text('miejsce');
        });
    }

    public function down()
    {
        Schema::dropIfExists('d_produkcja');
    }
}
