<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDSilnikTable extends Migration
{
    public function up()
    {
        Schema::create('d_silnik', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('moc');
            $table->string('pojemnosc');
        });
    }

    public function down()
    {
        Schema::dropIfExists('d_silnik');
    }
}
