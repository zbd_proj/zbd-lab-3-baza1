<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDKlientTable extends Migration
{
    public function up()
    {
        Schema::create('d_klient', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('wyksztalcenie')->nullable();
            $table->integer('wiek')->nullable();
            $table->string('plec')->nullable();
            $table->string('zarobki')->nullable();
            $table->string('kraj_pochodzenia')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('d_klient');
    }
}
