<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDMarkaTable extends Migration
{
    public function up()
    {
        Schema::create('d_marka', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nazwa');
        });
    }

    public function down()
    {
        Schema::dropIfExists('d_marka');
    }
}
