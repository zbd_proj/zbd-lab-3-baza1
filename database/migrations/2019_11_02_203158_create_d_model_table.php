<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDModelTable extends Migration
{
    public function up()
    {
        Schema::create('d_model', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nazwa');
            $table->bigInteger('marka_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('d_model');
    }
}
